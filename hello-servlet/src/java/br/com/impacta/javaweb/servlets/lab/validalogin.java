package br.com.impacta.javaweb.servlets.lab;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet(name="validalogin", urlPatterns={"/validalogin"})
public class validalogin extends HttpServlet {
 
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet validalogin</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet validalogin at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String senha = request.getParameter("senha");
        if (senha!=null && senha.equals("123")) {
            RequestDispatcher rs = request.getRequestDispatcher("/sistema");
            rs.forward(request, response);
        } else {
            response.sendRedirect("http://localhost:8080/hello-servlet/erroLogin.html");
        }       
    }
}
